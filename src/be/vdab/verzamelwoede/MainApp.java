package be.vdab.verzamelwoede;

import be.vdab.verzamelwoede.list.List;

import java.util.Arrays;

/**
 * @author Behlül Savaskurt
 * created on 17/06/2021
 */
public class MainApp {

    public static void main(String[] args) {

        List<String> array = new List<>();

        array.add("Hello");
        array.add("how");
        array.add("are");
        array.add("you?");
        System.out.println("Added objects: \n" + array);

        array.remove("are");
        System.out.println("Removed object: \n" + array);

        array.setObject(0, "Bonjour");
        System.out.println("Set object to index x: \n" + array);

        System.out.println("Get object at index x: \n" + array.getObject(2));

        List<String> array2 = new List<>();

        array2.add("I'm");
        array2.add("fine");
        array2.add("thanks");

        System.out.println("Second array: \n" + array2);

        array.addAll(array2);
        System.out.println("Concatenated 2 arrays: \n" + array);

        array.removeAll(array2);
        System.out.println("Removed second array: \n" + array);

        array.clear();
        System.out.println("Cleared array: \n" + array);

        System.out.println("Array is empty: \n" + array.isEmpty());
    }
}
