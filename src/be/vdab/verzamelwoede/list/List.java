package be.vdab.verzamelwoede.list;

import java.util.Arrays;

/**
 * @author Behlül Savaskurt
 * created on 17/06/2021
 */
public class List<T> {

    private Object[] objects;

    public List() {
        objects = new Object[0];
    }

    public boolean isEmpty() {
        /*if(objects.length == 0){
            return true;
        } else return false;*/

        for(Object object : objects) {
            if(object != null) {
                return false;
            }
        }
        return true;
    }

    public boolean contains(T o) {
        for(Object object : objects) {
            if(object == o) {
                return true;
            }
        }
        return false;
    }

    private void expandArray() {
        //maak nieuwe array met 1 extra ruimte
        Object[] newObjects = new Object[objects.length + 1];
        //voeg alle elementen uit objects toe aan de nieuwe array
        for (int i = 0; i < objects.length; i++) {
            newObjects[i] = objects[i];
        }
        //stel de nieuwe array gelijk aan objects
        objects = newObjects;
    }

    public void add(T o) {
        expandArray();
        //voeg object o achteraan de array toe
        objects[objects.length -1] = o;

//        int index;
//        for(index = 0; index < objects.length; index++) {
//            if(objects[index] == null) {
//                objects[index] = o;
//            }
//        }
    }

    public void addAll(List<T> array){ //array van objecten in de array steken
        for(int i = 0; i < array.objects.length; i++) {
            add((T) array.objects[i]);
        }
//        //maak nieuwe array aan met lengte van array1 + array2
//        Object[] newObjects = new Object[objects.length + array.length];
//
//        int newIndex = 0;
//        //kopieer elementen van array1 in de nieuwe array
//        for(Object object : objects) {
//            newObjects[newIndex] = object;
//            newIndex++;
//        }
//        //kopieer elementen van array2 in de nieuwe array
//        for(Object object : array) {
//            newObjects[newIndex] = object;
//            newIndex++;
//        }
//        objects = newObjects; //stel nieuwe array gelijk aan objects
    }

    public void remove(T o) {
        Object[] newObjects = new Object[objects.length -1];

        int newIndex = 0;
        //index van het te verwijderen element
        int index = indexOf(o);

        if(index != -1){ //als het element er niet inzit, valt er niets te verwijderen
            for (int i = 0; i < objects.length; i++) {
                if(index != i){ //als i == index: opgelet, dit element wordt niet toegevoegd
                    newObjects[newIndex] = objects[i];
                    newIndex++;
                }
            }
            objects = newObjects;
        }
    }

    public void removeAll(List<T> array) {
        for(int i = 0; i < array.objects.length; i++) {
            remove((T) array.objects[i]);
        }

//        Object[] newObjects = new Object[objects.length - array.length];
//
//        int newIndex = 0;
//        int index = indexOf(array);
//        if(index != -1){ //als het element er niet inzit, valt er niks te verwijderen
//            for (int i = 0; i < objects.length; i++) {
//                if(index != i){ //als i == index: opgelet, dit element wordt niet toegevoegd
//                    newObjects[newIndex] = objects[i];
//                    newIndex++;
//                }
//            }
//            objects = newObjects;
//        }
    }

    public void clear() {
        objects = new Object[0];
    }

    public int indexOf(T o) {
        for(int i = 0; i < objects.length; i++) {
            if(objects[i] == o) {
                return i;
            }
        }
        return -1;
    }

    public T getObject(int index) {
        return (T) objects[index];
    }

    public void setObject(int index, T o) {
        if(index <= objects.length) {
            objects[index] = o;
        }
    }

    @Override
    public String toString() {
        return "List{" +
                "objects=" + Arrays.toString(objects) +
                '}';
    }

}
